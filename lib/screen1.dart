import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Screen1 extends StatelessWidget {
  const Screen1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Hello SCreeen 1 !!!"),
            TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("Go Back")),
          ],
        ),
      ),
    );
  }
}
