import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/counter_page.dart';
import 'package:getx/home_page.dart';
import 'package:getx/screen1.dart';
import 'package:getx/slider_stateManagement.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      getPages: [
        GetPage(
          name: '/',
          page: () => HomePage(),
        ),
        GetPage(
          name: '/screenOne',
          page: () => Screen1(),
        ),
        GetPage(
          name: '/CounterPage',
          page: () => CounterPage(),
        ),
        GetPage(name: "/sliderExample", page:() => SliderExample(),),
      ],
      title: 'Flutter Demo',
      // theme: ThemeData(
      //   colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
      //   // useMaterial3: true,
      // ),
      home: const HomePage(),
    );
  }
}
