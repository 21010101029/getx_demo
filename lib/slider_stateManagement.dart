import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/SliderController.dart';

class SliderExample extends StatefulWidget {
  const SliderExample({super.key});

  @override
  State<SliderExample> createState() => _SliderExampleState();
}

class _SliderExampleState extends State<SliderExample> {
  final SliderController x=Get.put(SliderController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Slider Example ::"),
      ),
      body: Column(children: [
        Obx(
          () =>
           Center(
            child: Container(
              height: Get.height * .2,
              width: Get.width * .2,
              color: Colors.redAccent.withOpacity(x.x.value),
            ),
          ),
        ),
        Obx(
          () =>  Slider(onChanged: (value) {
                x.incrementSlider(value);
          },value: x.x.value,),
        )
      ]),
    );
  }
}
