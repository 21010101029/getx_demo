import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/CounterController.dart';

class CounterPage extends StatefulWidget {
  const CounterPage({super.key});

  @override
  State<CounterPage> createState() => _CounterPageState();
}

class _CounterPageState extends State<CounterPage> {
  final CounterController controller=Get.put(CounterController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Counter "),
      ),
      body: Obx(
        () =>
         Center(
          child: Text("${controller.x}"),
        ),
      ),
      floatingActionButton: FloatingActionButton(onPressed: () {
        controller.incrementCounter();
      },),
    );
  }
}
