import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("GETX"),
      ),
      body: Column(
        children: [
          Card(
            child: ListTile(
              title: Text("Alert Box"),
              onTap: () {
                Get.defaultDialog(
                    title: "Hello",
                    confirm: Text("OK"),
                    barrierDismissible: false);
              },
            ),
          ),
          Card(
            child: ListTile(
              title: Text("bottom sheet"),
              onTap: () {
                Get.bottomSheet(
                    Column(
                      children: [
                        ListTile(
                          onTap: () {
                            Get.changeTheme(ThemeData.light());
                          },
                          title: Text("Light"),
                        ),
                        ListTile(
                          onTap: () {
                            Get.changeTheme(ThemeData.dark());
                          },
                          title: Text("Dark"),
                        ),
                      ],
                    ),
                    backgroundColor: Colors.redAccent);
                // Get.defaultDialog(title: "Hello",confirm: Text("OK"),barrierDismissible: false);
              },
            ),
          ),
          Container(
            height: Get.height*.1,
            child: Card(
              child: ListTile(
                title: Text("Routes"),
                onTap: () {
                  Get.toNamed('/screenOne');
                },

              ),
            ),
          ),
          ListTile(title: Text("Counter StateManagement"),onTap: () {
            Get.toNamed("/CounterPage");
          },),
          ListTile(title: Text("Slider StateManagement"),onTap: () {
            Get.toNamed("/sliderExample");
          },)
        ],
      ),
    );
  }
}
