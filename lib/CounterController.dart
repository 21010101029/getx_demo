import 'package:get/get.dart';

class CounterController extends GetxController{
  RxInt x=1.obs;

  incrementCounter(){
    x.value++;
  }
}